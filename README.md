LinuxCNC EtherCAT blinking example
==================================

# Hardware setup

You'll need an EK1100 bus coupler and a EL2024 output clamp.
If you use other hardware, please adapt `my_ethercat.conf`.

# Installation

Grab a fresh copy of Debian 12, install a realtime-kernel and the IgH EtherCAT master. Follow [this guide](https://forum.linuxcnc.org/ethercat/45336-ethercat-installation-from-repositories-how-to-step-by-step?start=0).

Make sure that the `lcec` module is properly linked:
```
# ldd /usr/lib/linuxcnc/modules/lcec.so 
        linux-vdso.so.1 (0x00007ffff81c6000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f325b590000)
        liblinuxcnchal.so.0 => /lib/liblinuxcnchal.so.0 (0x00007f325b581000)
        libethercat.so.1 => /lib/x86_64-linux-gnu/libethercat.so.1 (0x00007f325b576000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f325b395000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f325b6ab000)
```

Also, install the blinking module using
```sh
sudo halcompile --install blink.comp
```

# Start the blinking application

Make sure the EtherCAT master is up and running. Then, execute `halrun -I -f blink.hal`. There might be a warning `cannot gain I/O privileges`, which can be ignored.

Have fun!
